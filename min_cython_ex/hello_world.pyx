

cdef extern from "test_source.cpp":
    int add(int a, int b)

def check_adding(param):
    cdef int c
    try:
        return add(param, param)
    except (AttributeError, TypeError):
        raise AssertionError('Input should be integer')
        
